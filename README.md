# Flectra Community / server-brand

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[disable_odoo_online](disable_odoo_online/) | 2.0.1.0.0| Remove flectra.com Bindings
[portal_odoo_debranding](portal_odoo_debranding/) | 2.0.1.0.1| Remove Odoo Branding from Portal
[remove_odoo_enterprise](remove_odoo_enterprise/) | 2.0.1.0.1| Remove enterprise modules and setting items


