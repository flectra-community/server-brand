# Copyright 2018 Eska Yazılım ve Danışmanlık A.Ş (www.eskayazilim.com.tr)
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Remove Odoo Enterprise",
    "summary": "Remove enterprise modules and setting items",
    "version": "2.0.1.0.1",
    "category": "Maintenance",
    "author": "Eska, Onestein, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/server-brand",
    "license": "AGPL-3",
    "depends": ["base"],
    "installable": True,
}
